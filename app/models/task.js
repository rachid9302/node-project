const mongoose = require('mongoose');

/********** IMPORTS *********/

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    title: { type: String, require:true },
    content: String,
    status: Boolean,
    createdAt: {type: Date, default: Date.now },
    updateAt: Date
});

module.exports = mongoose.model('Tasks',TaskSchema);