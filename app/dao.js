// Task Model
const Task = require('./models/task');
const Logger = require('./logger');

/* *** *** IMPORTS *** *** */
class DAO extends Logger{

    constructor() {
        super();
    }
    find() {
        return new Promise((resolve, reject) => {
           Task.find((err, tasks) => {
               if (err) {
                   this.log('FIND ERROR ${err}');
                   reject(err);
               }

               this.log('FIND SUCCESSFULLY');
               resolve(tasks);
            });
        });
    }
    findOne(id) {
        return new Promise((resolve, reject) => {
            Task.findById(id, (err, task) => {
            if (err) reject(err);

            resolve(task);
            });
        });
    }
}

module.exports = new DAO();
