//Check
const check = require('check-types');

// Task models
const Task = require('../models/task');
//const Logger = require('.logger');

/**********IMPORTS ********/

function checkProperties(body) {

    const request = {
        title: body.title,
        content: body.content,
        status: body.status
    };

    const checkTypes = {
        title: check.string,
        content: check.string,
        status: check.boolean
    };

    const params = check.map(request, checkTypes);

    // Map the object to Array of Values and Iterate over
    for (let param of Object.values(params))
        if (!param)
            return false;

    return true;
}

module.exports = {
    checkProperties: checkProperties
};