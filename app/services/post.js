//check
const check = require('../utilities/check');
//Task Model
const Task = require('../models/task');

/* *** *** IMPORTS *** *** */
function post(req, res) {
    if(!check.checkProperties(req.body))
        return res.sendStatus(400);

    const task = new Task();

    task.title = req.body.title;
    task.content = req.body.content;
    task.createAt = new Date();

    task.save((err, result) => {
        if (err) res.send(err);

        res.json({ message: 'Task created' });
    });
}

module.exports = post;