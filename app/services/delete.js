// Task Model
const Task = require('../models/task');

/* *** *** IMPORTS ** *** */

function deleteTask(req, res) {
    Task.remove({ _id: req.params.id }, err => {
        if (err) res.send(err);

        res.json({ message: 'Successfully deleted' });
    });
}

module.exports = {
    deleteTask: deleteTask
};