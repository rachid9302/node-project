//Task Model
//const Task = require('../models/task');

const DAO = require('../dao');

/* *** *** IMPORTS *** *** */

function initMessage() {
    return { message: 'Hello world!' };
}

function init(req,res) {
    const message = initMessage();
    res.json(message);
}

function getTasks(req, res) {
    DAO.find()
        .then(tasks => res.json(tasks))
        .catch(err => res.send(err));
}

function getTask(req, res) {
    const { id } = req.params;
    DAO.findOne(id)
        .then(task => res.json(task))
        .catch(err => res.send(err));
}

module.exports = {
    init: init,
    initMessage: initMessage,
    getTasks: getTasks,
    getTask: getTask
}
