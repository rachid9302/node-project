// Task Model
const Task = require('../models/task');

/* *** *** IMPORTS *** *** */
function updateTask(req, res) {
    Task.findById(req.params.id, function (err, task) {
        if (err) res.send(err);

        task.title = req.body.title;
        task.content = req.body.content;
        task.updatedAt = new Date();

        task.save((err, result) => {
            if (err)
                res.send(err);

            res.json({
                id: result._id,
                title: result.title,
                content: result.content,
                updateAt: result.updatedAt
            });
        });
    });
}

module.exports = {
    updateTask: updateTask
};