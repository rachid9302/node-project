const   express     = require('express'),
        bodyParser  = require('body-parser'),
        mongoose    = require('mongoose'),
        morgan      = require('morgan'),
        config      = require('config'),
        app         = express();

// API Service
const   serviceGet = require('./services/get'),
        servicePost = require('./services/post'),
        serviceUpdate = require('./services/update'),
        serviceDelete = require('./services/delete');

const API_CONFIG = config.api;

const PORT = process.env.PORT || API_CONFIG.port;

const router = express.Router();
app.use('/', router);

const db = mongoose.connection;

const HOST = `${config.db.host}${config.db.port}`;
mongoose.connect(HOST);

// Mongoose event emitter
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('DB Connected'));

// configure app
// log requests to the console
app.use(morgan('dev'));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requeted-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "*");
next();
});

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// API Routes
router.get('/', serviceGet.init);

app.use(`${API_CONFIG.path}${API_CONFIG.version}`, router);

router.route('/tasks')
    .post(servicePost)
    .get(serviceGet.getTasks);

router.route('/task/:id')
    .get(serviceGet.getTask)
    .put(serviceUpdate.updateTask)
    .delete(serviceDelete.deleteTask);

app.listen(PORT, () => {
    console.log(`running on : ${API_CONFIG.host}:${PORT}`);
});