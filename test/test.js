const expect = require('chai').expect;
const should = require('should');
const initMessage = require('../app/services/get').initMessage;
const logger = require('../app/logger');
const config = require('config');
const fs = require('fs');

const checkProperties = require('../app/utilities/check').checkProperties;
//const checkProperties = require('../app/utilities/check')
//const mapper = require('../app/utilities/mapper);

const assert = require('assert');

/* * * IMPORTS * * */

describe('First TEST always true', () => {
    it('shoud be true', () => expect(true).to.be.true);
});

describe('Test init message', () => {
    it('shoud be an object', () => expect( initMessage()).to.be.an('object'));
});

describe('Test Check Properties', () => {

    let mocks = {
        title: "mock",
        content: "msdq",
        status: false
    };

it('Test -!> should be true for check properties', () => {

    expect( checkProperties(mocks) ).to.be.true});

it('Test --!> should be false for check properties', () => {
    mocks.status = 'hello';

expect( checkProperties(mocks) ).to.be.false;
});


});

describe('Test Require Values', () => {

    let mocks = {
        title: "mock",
        content: "test de test",
        status: 1
    };

it('Test -!> should have the require values', () => {
    mocks.should.have.property('title');
});

it('Test --!> should not be empty', () => {
    expect(mocks).to.not.be.empty;
});

});